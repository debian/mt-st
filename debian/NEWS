mt-st (1.3-1) unstable; urgency=medium

    The algorithm for activating tape device parameters has changed
    significantly. In previous versions (up to and including 1.1-6), there
    were three things which were used to do this. First, an init script
    run during system boot (rcS) was setting parameters for all tapes if
    the `st' driver was built-in to the kernel. Second, a modprobe
    configuration file was shipped which modified the `st' module
    insertion to also execute stinit afterwards (via the init script with
    "modload" parameter). Third, an udev configuration rule was shipped
    which called the init script with the modload parameter.

    This was an extremely complicated way to do a simple thing: after each
    tape device activation, call stinit with the device path (irrespective
    of how `st' driver is built, and independent of the boot process). So
    the init script and modprobe configuration file were dropped, and only
    the udev rule was retained, which now directly calls `stinit
    $device'. If you don't run udev, you need to ensure calling once
    stinit post-boot (e.g. from rc.local) or post-device insertion
    (manuall).

 -- Iustin Pop <iustin@debian.org>  Wed, 04 May 2016 01:35:47 +0200
